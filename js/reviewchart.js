$(function () {
    $('#review-chart').highcharts({
        chart: {
                type: 'line'
        },
        title: {
            text: 'Reviewer: Reviewer1'
        },
        subtitle: {
            text: 'Date: 05/27/2017'
        },
        yAxis: {
            title: {
                text: 'Count'
            }
        },
         xAxis: {
            title: {
                text: 'Hours/ Days'
            }
        },
         series: [{
            name: 'Accept',
            data: [40, 80, 100, 90, 150, 130, 190, 60]
        }, {
            name: 'Reject',
            data: [4, 10, 6, 41, 21, 20, 2, 3]
        }]
    });
});