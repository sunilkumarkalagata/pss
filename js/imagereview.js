$(function () {
    $('#image-review-chart1').highcharts({
       // colors: ["#196F3D", "#C0392B"],
        chart: {
                type: 'column',
        },
       title: {
            text: 'Enqueue Details'
        },
        xAxis: {
            categories: [],
            crosshair: true
        },
        series: [{
            name: 'Inital',
            data: [5000, 8000, 9000, 4000]
        }, {
            name: 'DMV',
            data: [7000, 6890, 5000, 5800]
        }]
    });

    $('#image-review-chart2').highcharts({
    // colors: ["#196F3D", "#C0392B"],
        chart: {
                type: 'column',
        },
       title: {
            text: 'Dequeue Details'
        },
        xAxis: {
            categories: [],
            crosshair: true
        },
        series: [{
            name: 'Inital',
            data: [4000, 3000, 3600, 4000]
        }, {
            name: 'DMV',
            data: [3500, 3400, 3800, 3700]
        }]
    });
});